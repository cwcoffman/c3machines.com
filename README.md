# C3Machines.com
Static website for C3Machines marketing, including a brief bio on my career.  

The site is hosted on Amazon's (AWS) **S3** service.  This is a cheap, ultra-scalable, fault-tolerant way to host static pages.  As it is static, server-side code cannot be used, but Javascript, JQuery, and CSS may be used, as they run on the client browser.  It is a responsive site, meaning it should show well not only on different browsers, but on mobile devices as well, scaling to the screen size.

Other AWS technologies used include **Route 53** for DNS, **CloudFront** for TLS/SSL, **SNS** for monitoring changes to files and health checks, and **CloudWatch** for capturing metrics on site uptime and traffic.

The site is viewable at <a href="https://c3machines.com">c3machines.com or www.c3machines.com</a>.

This site is currently updated automatically through a Continuous Deployment lifecycle (simple) using a Jenkins pipeline project.

The "bucket-copy.txt" file contains some **AWS CLI** commands you can use to setup, copy and delete an S3 bucket.  I had originally not named my bucket appropriately for this use, and had to migrate files to a new bucket. Through the UI this is tedious and this will help if you find yourself in the same situation.

Thanks to <a href="http://html5up.net">HTML5 UP</a> for posting such a great template to use.  As a long-time web developer, I can admit that I am truly *not* a designer and appreciate the work that you share so freely with the world.
